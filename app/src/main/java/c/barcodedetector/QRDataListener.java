package c.barcodedetector;

/**
 * Created by MaltiDevnani on 1/11/2017.
 */

public interface QRDataListener {

    /**
     * On detected.
     *
     * @param data
     *     the data
     */
    // Called from not main thread. Be careful
    void onDetected(final String data);
}
