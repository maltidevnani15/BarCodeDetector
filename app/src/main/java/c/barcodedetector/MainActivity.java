package c.barcodedetector;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback {
    private SurfaceView mySurfaceView;
    private SurfaceHolder surfaceHolder;
    private QREader qrEader;

   private RelativeLayout rl;

    private ScrollView fragment_main_ll_user_info;
    private TextView fragment_main_tv_user_name;
    private TextView fragment_main_tv_unique_id;
    private TextView fragment_main_tv_original_amount;
    private TextView fragment_main_tv_redeem_date;
    private TextView fragment_main_tv_redeem_to_date;
    private TextView fragment_main_tv_remaining_amount;
    private TextView fragment_main_tv_dob;
    private TextView fragment_main_tv_mobile_number;
    private TextView fragment_main_tv_email;
    private ImageView fragment_main_iv_default;
    private LinearLayout fragment_main_ll_redeem;
    private EditText fragment_main_redeem_tv_amount;
    private TextView fragment_main_redeem_tv_submit;
    private TextView fragment_main_redeem_tv_error_message;
    private View divider_line;

    private TextView fragment_main_tv_redeem;
    private TextView activity_main_tv_scan_now;
    private TextView activity_main_tv_stop;
    private TextView activity_main_tv_scan_gift_card;
    private TextView activity_main_tv_manually_enter_card;
    private TextView activity_main_tv_support;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        checkCameraPermission();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rl=(RelativeLayout)findViewById(R.id.rl);
    mySurfaceView = (SurfaceView) findViewById(R.id.camera_view);
        surfaceHolder = mySurfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.lockCanvas();

        // Init QREader
        // ------------
        qrEader = new QREader.Builder(this, mySurfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                Log.e("QREader", "Value : " + data);
                activity_main_tv_scan_now.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,data,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(mySurfaceView.getHeight())
                .width(mySurfaceView.getWidth())
                .build();

        final String scan_text = "<font color=#ffffff>SCAN</font>  <font color=#00d17d>FLUNKY</font>  <font color=#ffffff>GIFT CARD</font>";
        final String manual_text = "<font color=#ffffff>MANUALLY ENTER</font> <font color=#00d17d>FLUNKY</font>  <font color=#ffffff>GIFT CARD</font>";
        final String flunkySupport_text = " <font color=#00d17d>FLUNKY</font>  <font color=#ffffff>SUPPORT</font>";
//        mScannerView = new ZXingScannerView(getContext());

//        mScannerView.setResultHandler(this);
        fragment_main_ll_user_info = (ScrollView)findViewById(R.id.fragment_main_ll_user_info);
        fragment_main_tv_user_name = (TextView)findViewById(R.id.fragment_main_tv_user_name);
        fragment_main_tv_unique_id = (TextView)findViewById(R.id.fragment_main_tv_unique_id);
        fragment_main_tv_redeem_date = (TextView)findViewById(R.id.fragment_main_tv_redeemDate);
        fragment_main_tv_redeem_to_date = (TextView)findViewById(R.id.fragment_main_tv_redeemToDate);
        fragment_main_tv_remaining_amount = (TextView)findViewById(R.id.fragment_main_tv_remainingAmount);
        fragment_main_tv_dob = (TextView)findViewById(R.id.fragment_main_tv_dob);
        fragment_main_tv_mobile_number = (TextView)findViewById(R.id.fragment_main_tv_movileNumber);
        fragment_main_tv_email = (TextView)findViewById(R.id.fragment_main_tv_email);
        fragment_main_tv_redeem = (TextView) findViewById(R.id.fragment_main_tv_redeem);

        fragment_main_ll_redeem = (LinearLayout)findViewById(R.id.fragment_main_ll_redeem);
        fragment_main_redeem_tv_amount = (EditText)findViewById(R.id.fragment_main_redeem_tv_amount);
        fragment_main_redeem_tv_submit = (TextView)findViewById(R.id.fragment_main_redeem_tv_submit);
        fragment_main_redeem_tv_error_message = (TextView)findViewById(R.id.fragment_main_redeem_tv_error);

        fragment_main_iv_default = (ImageView) findViewById(R.id.fragment_main_iv_default_image);
        divider_line = findViewById(R.id.divider_line);


        activity_main_tv_scan_now = (TextView)findViewById(R.id.activity_main_tv_scan_now);
        activity_main_tv_stop = (TextView)findViewById(R.id.activity_main_tv_stop);
        activity_main_tv_scan_gift_card = (TextView) findViewById(R.id.activity_main_tv_scan_gift_card);
        activity_main_tv_manually_enter_card = (TextView) findViewById(R.id.activity_main_tv_manually_enter);
        activity_main_tv_support = (TextView)findViewById(R.id.activity_main_tv_flunky_support);

        activity_main_tv_scan_gift_card.setText(Html.fromHtml(scan_text));
        activity_main_tv_manually_enter_card.setText(Html.fromHtml(manual_text));
        activity_main_tv_support.setText(Html.fromHtml(flunkySupport_text));

        fragment_main_redeem_tv_submit.setOnClickListener(this);
        fragment_main_tv_redeem.setOnClickListener(this);
        activity_main_tv_scan_now.setOnClickListener(this);
        activity_main_tv_stop.setOnClickListener(this);
        activity_main_tv_scan_gift_card.setOnClickListener(this);
        activity_main_tv_manually_enter_card.setOnClickListener(this);
        activity_main_tv_support.setOnClickListener(this);
    }
    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    1001);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1001: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    checkCameraPermission();
                }
                return;
            }

        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        // Init and Start with SurfaceView
        // -------------------------------
        qrEader.initAndStart(mySurfaceView);
    }
    @Override
    protected void onPause() {
        super.onPause();

        // Cleanup in onPause()
        // --------------------

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_tv_scan_now:
                setView();
                break;
            case R.id.activity_main_tv_stop:
                rl.setVisibility(View.GONE);
                fragment_main_ll_user_info.setVisibility(View.GONE);
                fragment_main_iv_default.setVisibility(View.VISIBLE);
                activity_main_tv_scan_now.setVisibility(View.VISIBLE);
                activity_main_tv_stop.setVisibility(View.GONE);

                break;
            case R.id.activity_main_tv_scan_gift_card:
              callMailClient();
                break;
            case R.id.fragment_main_tv_redeem:
                fragment_main_ll_redeem.setVisibility(View.VISIBLE);
                fragment_main_redeem_tv_error_message.setVisibility(View.VISIBLE);
                fragment_main_redeem_tv_error_message.setText("");
                fragment_main_redeem_tv_amount.setText("");
                break;
            case R.id.fragment_main_redeem_tv_submit:

                break;

        }

    }
    private void callMailClient() {

            final String []To={"yourvoucher@flunky.com"};
            Intent voucheremailIntent = new Intent(Intent.ACTION_SEND);
            voucheremailIntent.setType("text/plain");
            voucheremailIntent.putExtra(Intent.EXTRA_EMAIL,To);
            voucheremailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact For Voucher Information");
            startActivity(voucheremailIntent);

    }
    private void setView() {
        fragment_main_ll_user_info.setVisibility(View.GONE);
        fragment_main_ll_redeem.setVisibility(View.GONE);
        fragment_main_iv_default.setVisibility(View.GONE);
        activity_main_tv_scan_now.setVisibility(View.GONE);

        activity_main_tv_stop.setVisibility(View.VISIBLE);
        divider_line.setVisibility(View.VISIBLE);
        // Register ourselves as a handler for scan results.
     rl.setVisibility(View.VISIBLE);

//        Intent intent = new Intent();
//        intent.setAction("com.google.zxing.client.android.SCAN");
//        startActivityForResult(intent, 0);


    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder=mySurfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        qrEader = new QREader.Builder(this, mySurfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                Log.e("QREader", "Value : " + data);
                activity_main_tv_scan_now.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,data,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(mySurfaceView.getHeight())
                .width(mySurfaceView.getWidth())
                .build();


    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        qrEader.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
